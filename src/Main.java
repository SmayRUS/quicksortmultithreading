import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;

public class Main {
    public static void main(String args[]) {
        int n = 100;
        int[] arr = new int[n];
        int[] unsorted = new int[n];

        Random randomizer = new Random();

        for ( int i = 0; i < n; i++ ) {
            arr[i] = randomizer.nextInt( 10_00000 );
            unsorted[i] = arr[i];
        }

        ForkJoinPool pool = ForkJoinPool.commonPool();

        long start = System.currentTimeMillis();
        pool.invoke(new QuickSortMultiThreading(0, n - 1, arr));


        long finish = System.currentTimeMillis();
        long elapsed = finish - start;

        System.out.println("QuickSortMultiThreading: " + elapsed + " milliseconds");

//        for (int i = 0; i < n; i++)
//            System.out.print(arr[i] + " ");

        start = System.currentTimeMillis();
        Arrays.sort(unsorted);
        finish = System.currentTimeMillis();
        elapsed = finish - start;

        System.out.println("");
        System.out.println("Arrays sort: " + elapsed + " milliseconds");
    }
}
